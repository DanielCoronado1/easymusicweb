<footer class="container-fluid text-center" class="footer">
<div class="container">
  	 	<div class="row">
  	 		<div class="footer-col">
  	 			<h4>Creadores</h4>
  	 			<ul>
  	 				<li><a href="#">Ricardo Daniel Coronado Martinez</a></li>
  	 				<li><a href="#">José Eugenio García Sánchez </a></li>
  	 				<li><a href="#">Johana Abigail Sánchez Domínguez </a></li>

  	 			</ul>
  	 		</div>
         <div class="footer-col">
  	 			<h4>contactanos</h4>
  	 			<ul>
            <li><a href="">Mision</a></li>
            <li><a href="">Vision</a></li>
            <li><a href="">Valores</a></li>
          </ul>
  	 		</div>
  	 		<div class="footer-col">
  	 			<h4>Ubicacion</h4>
  	 			<ul>
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3595.365133645633!2d-100.5140452856618!3d25.69233201774021!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86629ecd749f16df%3A0x525aada7e2a78b2c!2sUniversidad%20Tecnol%C3%B3gica%20Santa%20Catarina!5e0!3m2!1ses-419!2smx!4v1655337486158!5m2!1ses-419!2smx" width="150" height="150" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
  	 			</ul>
  	 		</div>
  	 		<div class="footer-col">
  	 			<h4>Redes Sociales</h4>
  	 			<div class="social-links">
  	 				<a href="#"><i class="fab fa-facebook-f"></i></a>
  	 				<a href="#"><i class="fab fa-twitter"></i></a>
  	 				<a href="#"><i class="fab fa-instagram"></i></a>
  	 				<a href="#"><i class="fab fa-linkedin-in"></i></a>
  	 			</div>
  	 		</div>
  	 	</div>
  	 </div>
</footer>